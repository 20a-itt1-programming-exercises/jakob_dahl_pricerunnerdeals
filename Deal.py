deals = []          # List to keep all Deal objects in
dealsLink = []      # List to keep all links to the deals

class DealData:
    def __init__(self, productname, newprice, oldprice, link, imglink, seller, sellerimglink, categories, lowestprice):
        self.productname = productname
        self.newprice = newprice
        self.oldprice = oldprice
        self.link = link
        self.imglink = imglink
        self.seller = seller
        self.sellerimglink = sellerimglink
        self.categories = categories
        self.lowestprice = lowestprice