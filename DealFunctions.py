# Importing webdriver from the selenium library which "drives the website" in other words it is used to navigate, give input and retrieve information from/on the website
from selenium import webdriver
# Importing keys from the selenium library which is able to send inputs such as PAGE_DOWN or HOME to the website
from selenium.webdriver.common.keys import Keys
# Importing By from the selenium library which is used to specfify on how the element shall be retrieved (XPATH, Class, ID...)
from selenium.webdriver.common.by import By
# Importing WebDriverWait from the selenium library which is used to "stop the thread" until the given element is visible on the web page.
from selenium.webdriver.support.ui import WebDriverWait
# Importing expected_conditions from the selenium library which is also used for the "wait until element is visible" method
from selenium.webdriver.support import expected_conditions as EC
# Importing my own python module that contains the class object "DealData" which is used to store all the data that is retrieved for each deal.
from Deal import *
# Importing time which is used to stop the thread for a specific time, and also used to get the current date.
import time

# initializes the priceruner website as a variable
website = "https://www.pricerunner.dk/deals"

# initializes the current date as a variable
todaysDate = time.strftime("%d-%m-%y")

# initializes the ChromeDriver that controls the browser as a variable
driver = webdriver.Chrome('./chromedriver')  

# Used for waiting for elements to be visible 
wait = WebDriverWait(driver,5)                      

def Setup():

    # Reads the content of the "login.txt" file which contains a username and password to logon pricerunner.dk
    f = open("login.txt", "r")
    content = f.read()
    # Splits the content of the "login.txt" into seperate lines
    loginInfo = content.splitlines()
    f.close()

    # Opens https://www.pricerunner.dk/deals with the ChromeDriver
    driver.get(website)  
    # Clicks on "Afvis alle" if it's visible.
    afvis_alle = driver.find_element_by_xpath('//*[@id="root"]/div[1]/div/div[2]/div[1]/div[2]/div/button[2]')
    if afvis_alle.is_displayed():
        afvis_alle.click()
    # Clicks on "Log ind"
    driver.find_element_by_xpath('//*[@id="root"]/div[2]/header/div/div/div[3]/div/div/div[1]/div[2]/button').click()
    # Clicks on "Fortsæt med e-mail"
    wait.until(EC.visibility_of_element_located((By.XPATH,'/html/body/div[2]/div[2]/div/div/div[5]/button'))) 
    driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div/div[5]/button').click()
    # Enters email
    wait.until(EC.visibility_of_element_located((By.XPATH,'//*[@id="userdialog.login.email"]'))) 
    driver.find_element_by_xpath('//*[@id="userdialog.login.email"]').send_keys(loginInfo[0])
    # Enters password
    driver.find_element_by_xpath('//*[@id="userdialog.login.password"]').send_keys(loginInfo[1])
    # Clicks on "Log ind"
    driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/div/form/div[4]/button').click()
    # Selects "Spar 50% eller mere"
    driver.find_element_by_xpath('//*[@id="deal-of-the-day"]/div[1]/div/div/div[2]/div[1]/div/div/div/ul/fieldset/li[1]/label/div/span[2]/span/p[1]').click()                                                                  

def GetNumOfDeals():

    # Gets the amount of deals with a saving of 50% or more.
    time.sleep(0.25)
    allDeals = int(driver.find_element_by_xpath('//*[@id="deal-of-the-day"]/div[1]/div/div/div[2]/div[1]/div/div/div/ul/fieldset/li[1]/label/div/span[2]/span/p[2]').text)      
    print(f"Found: {allDeals} deals!")
    return allDeals

def DealUrlTemplate(dealIdx):

    # Gets the dealUrl
    dealUrl = f'//*[@id="deals-of-the-day-container"]/div/div/div[{dealIdx}]/div/a'
    return dealUrl

def GetDealsLink(allDeals):

    retryAttempts = 0

    # Scrolls to the bottom of the deals page
    htmlElem = driver.find_element_by_css_selector('html')
    htmlElem.send_keys(Keys.END)

    # Goes through every deal and gets their link
    for dealNum in range(20): 
        dealNum += 1
        # A maximum of 60 deals is shown at the time, therefor when the dealNum iterator is divided with 60 and returns a integer it scrolls to the bottom of the page.
        if (dealNum / 60).is_integer():
            htmlElem.send_keys(Keys.END)
        try:
            # Gets deal url
            dealUrl = DealUrlTemplate(dealNum)                                          
            # Waits until the url for the current deal iteration is visible
            wait.until(EC.visibility_of_element_located((By.XPATH,dealUrl))).get_property("href") 
            # Stores the deals pricerunner url
            dealLink = driver.find_element_by_xpath(dealUrl).get_property("href")   
            # Adds the deal url to a list
            dealsLink.append(dealLink)
            print(f"Found link for deal number {dealNum}: {dealLink}")
            
        except Exception as e:
            retryAttempts += 1
            print(f"Error occured for deal[{dealNum}]: {e}\nTrying again - {retryAttempts}!")
            # Scrolls down to the end of page in attempt of finding the deal
            htmlElem = driver.find_element_by_css_selector('html')
            htmlElem.send_keys(Keys.END)
            htmlElem.send_keys(Keys.END)

        if retryAttempts == 5:
            break
            print("Couldn't find deal {dealNum} - stopping the search for further deals")

    return dealsLink

def GetLinkFrom3rdParty(search):

    # Opens "google.dk"
    driver.get("https://www.google.dk:")
    try:
        # Removes the cookies pop up from google
        js_string = "var element = document.getElementById(\"main\");element.remove();"
        driver.execute_script(js_string)
        # Searches google with company selling the product and name of product
        searchField = driver.find_element_by_name('q')
        searchField.send_keys(search)
        searchField.send_keys(Keys.RETURN)
        # Removes the cookies pop up from google
        js_string = "var element = document.getElementById(\"lb\");element.remove();"
        driver.execute_script(js_string)
        # Gets the first link from the search result
        deal3rdPartyLink = driver.find_element_by_xpath('//*[@id="rso"]/div[1]/div/div[1]/a').get_property("href")
        linkIdx = 1
        driver.execute_script("document.body.style.zoom='50%'")
        while True:
            if linkIdx == 10:
                break
            try:
                # If the link is to pricerunner, it tries the next link from the search result 
                if "pricerunner" in deal3rdPartyLink:
                    linkIdx += 1
                    deal3rdPartyLink = driver.find_element_by_xpath(f'//*[@id="rso"]/div[{linkIdx}]/div/div[1]/a').get_property("href")
                else:
                    break
            except:
                continue
    except Exception as e:
        print(f"Error occured while searching for {search}]: {e}")
    driver.back()
    driver.back()

    return deal3rdPartyLink

def GetDeals(dealsLink):
        
    for dealLink in dealsLink:   

        # Opens the link to the deal
        driver.get(dealLink)

        # Gets the name of the product
        dealProductname = driver.find_element_by_xpath('//*[@id="product-body"]/div[2]/div/div[1]/div[2]/div[2]/div[1]/div[1]/h1').text  

        # Gets the categories which the product is under
        dealCategories = tuple(driver.find_element_by_xpath('//*[@id="product-body"]/div[2]/div/div[1]/div[1]/ol').text.split('\n'))

        # Gets the link to the image of the product
        try:
            dealImglink = driver.find_element_by_xpath('//*[@id="product-body"]/div[2]/div/div[1]/div[2]/div[1]/div/div[2]/img').get_property("srcset")[:-3]
        except:
            dealImglink = driver.find_element_by_xpath('//*[@id="product-body"]/div[2]/div/div[1]/div[2]/div[1]/div/div/img').get_property("srcset")[:-3]          
            
        # Gets the current price of the product
        try:
            dealNewprice = int(driver.find_element_by_xpath('//*[@id="product-body"]/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/a/div[2]/span').text[:-3].replace('.',''))                                        
        except:
            try:
                dealNewprice = int(driver.find_element_by_xpath('//*[@id="product-body"]/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div/div[2]/a/div[2]/span').text[:-3].replace('.',''))
            except:
                try:
                    dealNewprice = int(driver.find_element_byxpath('//*[@id="product-body"]/div[3]/div/div/div[2]/div/div/div[2]/div/div/div/div[2]/a/div[2]/span').text[:-3].replace('.',''))
                except:
                    dealNewprice = int(driver.find_element_by_xpath('//*[@id="product-body"]/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/span').text[3:-4].replace('.',''))          
        
        # Gets the name of the seller which is selling the product at the lowest price
        try:
            dealSeller = driver.find_element_by_xpath('//*[@id="product-body"]/div[3]/div/div/div[2]/div/div/div[2]/div[2]').get_attribute("aria-label").split(',')[0].split('.')[0]   
        except:
            try:
                dealSeller = driver.find_element_by_xpath('//*[@id="product-body"]/div[3]/div/div/div[2]/div/div/div[2]/div[1]').get_attribute("aria-label").split(',')[0].split('.')[0]
            except:
                dealSeller = driver.find_element_by_xpath('//*[@id="product-body"]/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[1]/div/p').text

        # Gets the link to where the product is being sold at the lowest price
        try:
            dealLink = driver.find_element_by_xpath('//*[@id="product-body"]/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div[2]/a').get_property("href")
        except:
            dealLink = GetLinkFrom3rdParty(f"{dealSeller} {dealProductname}")

        # Gets the link to the image of the seller, selling the product at the lowest price
        try:
            dealSellerimglink = driver.find_element_by_xpath('//*[@id="product-body"]/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[1]/a/div/div[1]/span/div/img').get_property("src")
        except:
            dealSellerimglink = "N/A"

        try:
            # Clicks on 'Statistik'
            driver.find_element_by_xpath('//*[@id="product-listing-navigation"]/div/ul/li[3]/button/span/span').click()                                                                                 
            wait.until(EC.visibility_of_element_located((By.XPATH,'//*[@id="pricegraph"]/div/div/div[1]/div/div[2]/div/label[2]/span/div')))
            time.sleep(0.5)

            # Clicks on shows earlier prices as a list
            driver.find_element_by_xpath('//*[@id="pricegraph"]/div/div/div[1]/div/div[2]/div/label[2]/span/div').click()

            # Gets the last price, that the product was being sold at
            dealOldprice = int(driver.find_element_by_xpath('//*[@id="pricegraph"]/div/div/div[3]/div[2]/div[2]/div/div[3]/span').text[:-3].replace('.',''))
        except Exception as e:
            print(f"Couldn't get old price for {dealProductname} - Error: {e}")

        try:
            # Clicks on 'Vis mere'
            driver.find_element_by_xpath('//*[@id="pricegraph"]/div/div/div[4]/button').click()                                                                                                        
        except:
            pass

        try:
            # Gets all previous prices of the product
            pricehistoryList = driver.find_element_by_xpath('//*[@id="pricegraph"]/div/div/div[3]/div[2]')
            pricehistoryChildren = pricehistoryList.find_elements_by_xpath("//*[starts-with(@class,'QQjPql6n8R')]")
            dealLowestpriceList = []
            # Adds all individual prices to a list
            for price in pricehistoryChildren[::2]:
                price = int(price.text[:-3].replace('.',''))
                dealLowestpriceList.append(price)
                time.sleep(0.01)
            # Gets the historically lowest price listed on pricerunner that the product was sold at
            dealLowestprice = min(dealLowestpriceList) 
        except Exception as e:
            print(f"Couldn't get lowest price for {dealProductname} - Error: {e}")

        # Calculates the actual saving, compared to the last price, that the product was being sold at
        dealSaving = round((dealOldprice - dealNewprice) / dealOldprice,2)*100
        
        # Stores all the information about the product to a list
        deals.append(DealData(dealProductname, dealNewprice, dealOldprice, dealLink, dealImglink, dealSeller, dealSellerimglink, dealCategories,dealLowestprice))
        time.sleep(0.1)

    return deals

def ReturnAllDeals(deals):

    # Composes a new .md file containing all the deals with their respective information/data.
    f = open(f"./Deals/Deals_{todaysDate}.md", "w")
    f.write(f"# Deals - {todaysDate}")
    f.close()
    
    for deal in deals:
        f = open(f"./deals/deals_{todaysDate}.md", "a")
        print(f"{deal.productname}\n\n{deal.newprice}\n{deal.oldprice}\n{deal.link}\n{deal.imglink}\n{deal.seller}\n{deal.sellerimglink}\n{deal.categories}\n{deal.lowestprice}\n\n")
        f.write(f"\n\n### [{deal.productname} - sold by {deal.seller}]({deal.link})\n<img src='{deal.sellerimglink}' width='10%' height='10%'/>\n<br>\n<img src='{deal.imglink}' width='25%' height='25%'/>\n\n### <strong>{deal.newprice} kr.</strong>\n\n#### <s>{deal.lowestprice} kr.</s>\n\n> #### {deal.categories}\n\n")
        f.close()

def DriverClose():
    # Closes the driver when the program is done
    driver.close()
    print("\nCompleted!\n")